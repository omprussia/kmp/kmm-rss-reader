/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
import QtQuick 2.0
import Sailfish.Silica 1.0

Rectangle {
    id: root

    property bool isCurrent

    signal clicked

    width: parent.height
    height: parent.height
    radius: parent.height
    border {
        width: root.isCurrent ? Theme.itemSizeMedium / 15 : 0
        color: Theme.highlightColor
    }

    BackgroundItem {
        anchors.fill: parent
        contentItem.radius: parent.height

        onClicked: root.clicked()

        Label {
            anchors.centerIn: parent
            text: qsTr("All")
            color: highlighted ? Theme.highlightColor : "black"
        }
    }
}
