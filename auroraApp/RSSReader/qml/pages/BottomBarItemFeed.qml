/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
import QtQuick 2.0
import Sailfish.Silica 1.0
import QtGraphicalEffects 1.0

Rectangle {
    id: root

    property bool isCurrent

    signal clicked

    width: parent.height
    height: parent.height
    radius: root.width
    color: 'transparent'
    layer {
        enabled: true
        effect: OpacityMask {
                maskSource: Item {
                    width: root.width
                    height: root.height
                    Rectangle {
                        anchors.fill: parent
                        radius: root.width
                    }
                }
            }
    }

    Rectangle {
        anchors.fill: parent
        color: Theme.secondaryColor
        radius: root.width
    }

    BusyIndicator {
        anchors.centerIn: parent
        size: BusyIndicatorSize.Small
        color: "black"
        visible: itemImage.status == Image.Loading
        running: itemImage.status == Image.Loading
    }

    Image {
        id: itemImage

        anchors.fill: parent
        source: Qt.resolvedUrl(imageUrl)
        fillMode: Image.PreserveAspectCrop
    }

    Rectangle {
        anchors.fill: parent
        color: "transparent"
        radius: root.width
        border {
            width: root.isCurrent ? Theme.itemSizeMedium / 15 : 0
            color: Theme.highlightColor
        }
    }

    BackgroundItem {
        anchors.fill: parent
        contentItem.radius: parent.height

        onClicked: root.clicked()
    }
}
