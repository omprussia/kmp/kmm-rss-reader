/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
import QtQuick 2.0
import Sailfish.Silica 1.0

ListItem {
    id: root

    signal deleteItem(string url)

    function removeItem() {
        remorseAction(qsTr("Removing a feed"), function() {
            root.deleteItem(model.sourceUrl)
        })
    }

    enabled: model.index !== 0
    opacity: enabled ? 1.0 : Theme.opacityLow
    width: parent.width
    contentHeight: content.height + Theme.paddingMedium * 2
    openMenuOnPressAndHold: model.index !== 0
    menu: ContextMenu {
        MenuItem {
            text: qsTr("Delete")
            onClicked: removeItem()
        }
    }

    Behavior on opacity { FadeAnimation {} }

    Column {
        id: content

        anchors.centerIn: parent
        width: parent.width - Theme.paddingLarge * 2
        spacing: Theme.paddingMedium

        Text {
            width: parent.width
            text: model.title
            wrapMode: Text.WordWrap
            color: root.highlighted ? Theme.highlightColor : Theme.primaryColor
            textFormat: Text.StyledText
            horizontalAlignment: Qt.AlignLeft
            font.pixelSize: Theme.fontSizeLarge
            font.weight: Font.Medium
        }

        Text {
            width: parent.width
            text: model.desc
            color: root.highlighted ? Theme.highlightColor : Theme.primaryColor
            wrapMode: Text.WordWrap
            textFormat: Text.StyledText
            horizontalAlignment: Qt.AlignLeft
            font.pixelSize: Theme.fontSizeSmall
        }
    }
}
