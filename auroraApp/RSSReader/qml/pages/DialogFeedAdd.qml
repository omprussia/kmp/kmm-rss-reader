/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
import QtQuick 2.0
import Sailfish.Silica 1.0

Dialog {
    id: root

    property string name

    readonly property var _regExp: new RegExp('^(?!mailto:)(?:(?:http|https|ftp)://)(?:\\S+(?::\\S*)?@)?(?:(?:(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[0-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-z\\u00a1-\\uffff]{2,})))|localhost)(?::\\d{2,5})?(?:(/|\\?|#)[^\\s]*)?$', 'i')

    signal submit(string url)

    canAccept: false
    allowedOrientations: Orientation.All

    onDone: {
        if (result == DialogResult.Accepted) {
            root.submit(field.text)
        }
        field.text = ""
        field.focus = false
    }

    Column {
        width: parent.width

        DialogHeader {
            title: qsTr("New feed")
            acceptText: qsTr("Add")
        }

        TextField {
            id: field

            width: parent.width
            placeholderText: qsTr("Feed URL")
            inputMethodHints: Qt.ImhUrlCharactersOnly
            acceptableInput: text.length === 0 || _regExp.test(text)

            EnterKey.enabled: text.length !== 0 && _regExp.test(text)
            EnterKey.iconSource: "image://theme/icon-m-accept"
            EnterKey.onClicked: root.accept()

            onTextChanged: root.canAccept = _regExp.test(text)
        }
    }
}
