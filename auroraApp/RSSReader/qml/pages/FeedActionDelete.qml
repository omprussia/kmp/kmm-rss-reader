/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
import QtQuick 2.0
import Sailfish.Silica 1.0

Item {
    id: root

    property string name: ""

    signal submit

    anchors.fill: parent

    Row {
        id: header

        spacing: Theme.paddingMedium

        Icon {
            id: icon

            width: label.height
            height: label.height
            source: "image://theme/icon-s-high-importance"
            color: Theme.primaryColor
        }

        Label {
            id: label

            text: qsTr("Delete")
        }
    }

    Item {
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: button.top
        }

        Text {
            anchors.centerIn: parent
            width: parent.width
            text: qsTr('Are you sure you want to delete rss feed "' + name + '"')
            color: Theme.primaryColor
            wrapMode: Text.WordWrap
            textFormat: Text.StyledText
            horizontalAlignment: Qt.AlignLeft
            font.pixelSize: Theme.fontSizeSmall
        }
    }

    Button {
        id: button

        anchors {
            bottom: parent.bottom
            right: parent.right
        }
        text: qsTr("Yes, delete")

        onClicked: root.submit()
    }
}
