/**
 * SPDX-FileCopyrightText: Copyright 2023 Open Mobile Platform LLC <community@omp.ru>
 * SPDX-License-Identifier: BSD-3-Clause
 */
import shared from "shared";

/**
 * RssReader js
 */
export const JsRssReader = new shared.com.github.jetbrains.rssreader.core.JsRssReader()
